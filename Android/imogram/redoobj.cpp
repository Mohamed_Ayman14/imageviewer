#include "redoobj.h"

redoObj::redoObj(int op, double zFact, int pDeg, int sx, int sy, int ex, int ey, bool gr, int hs, int vs)
{
    operation = op;
    zoomFactor = zFact;
    previousDeg = pDeg;
    startX = sx;
    startY = sy;
    endX = ex;
    endY = ey;
    gray = gr;
    hslider = hs;
    vslider = vs;
 }
