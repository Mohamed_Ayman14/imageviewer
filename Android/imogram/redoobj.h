#ifndef REDOOBJ_H
#define REDOOBJ_H


class redoObj
{
public:
    redoObj(int op, double zFact, int pDeg, int sx, int sy, int ex, int ey, bool gr, int hs, int vs);

public:
    int operation;
    double zoomFactor;
    int previousDeg;
    int startX, startY, endX, endY;
    bool gray;
    int vslider, hslider;
 };

#endif // REDOOBJ_H
