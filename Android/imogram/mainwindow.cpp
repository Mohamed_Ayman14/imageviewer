#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QApplication>
#include <QFileDialog>
#include <QScrollBar>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QtCore/qmath.h>
#include <QImage>
#include <QDebug>
#include <stack>

#define INITIAL 0
#define CROP 1
#define ZOOM 2
#define GRAY 3
#define ROTATE 4

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setMouseTracking(true);
    this->setWindowTitle("ImoGram");

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    QIcon icon("../Pictures/icon.png");
    setWindowIcon(icon);

    resetValues(1);
    start = false;

    cimgx1 = cimgx2 = cimgy1 = cimgy2 = 0;

    ui->horizontalSlider->setRange(0, 360);
    ui->graphicsView->viewport()->installEventFilter(this);
    QObject::connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(rotateImg(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QFileDialog *dialog = new QFileDialog(this);
    dialog->setFileMode( QFileDialog::AnyFile );
    dialog->setNameFilter(tr("Images (*.png *.bmp *.jpg *.jpeg)"));
    dialog->setViewMode(QFileDialog::List);
    QStringList fileNames;
    if(dialog->exec()){
        fileNames = dialog->selectedFiles();
        addImage(fileNames[0]);
    }
}
void MainWindow::rotateImg(int degree)
{
    if(!start){
        resetValues(1);
        return;
    }
    previousDeg = degree;

    setLabelsValues();
    selected = false;
    reDraw(1);
}


//Add image
void MainWindow::addImage(QString path) {

    resetValues(1);
    cimgx1 = cimgx2 = cimgy1 = cimgy2 = 0;
    start = true;

    ui->graphicsView->resetTransform();

    image = new QImage();

    originalImage = new QImage();
    originalImage->load(path);

    image->load(path);
    pix_image = QPixmap::fromImage(*image);

    scene->clear();
    scene->addPixmap(pix_image);
    scene->setSceneRect(pix_image.rect());

    while(undo.size() > 0){
        undo.pop();
    }
    emptyRedo();
    redoObj *Ob = new redoObj(INITIAL,1,0,0,0,0,0,false, ui->graphicsView->horizontalScrollBar()->value(), ui->graphicsView->verticalScrollBar()->value());
    undo.push(Ob);
}

//Zoom in button
void MainWindow::on_pushButton_3_clicked()
{
    if(!start)
        return;
    if(zoomFactor < 5){

        zoomFactor += 1;

        reDraw(1);
        setLabelsValues();

        if(selected){

            drawRectangle(rect->topLeft().x()*zoomFactor/(zoomFactor-1), rect->topLeft().y()*zoomFactor/(zoomFactor-1),
                          rect->bottomRight().x()*zoomFactor/(zoomFactor-1), rect->bottomRight().y()*zoomFactor/(zoomFactor-1));

            ui->graphicsView->horizontalScrollBar()->setValue(rect->topLeft().x()-10);
            ui->graphicsView->verticalScrollBar()->setValue(rect->topLeft().y()-10);
        }
        redoObj *Ob = new redoObj(ZOOM, zoomFactor, previousDeg, cimgx1, cimgy1, cimgx2, cimgy2, gray, (int)ui->graphicsView->horizontalScrollBar()->value(), (int)ui->graphicsView->verticalScrollBar()->value());
        undo.push(Ob);
        emptyRedo();
    }

}

//Zoom out button
void MainWindow::on_pushButton_4_clicked()
{
    if(!start)
        return;
    if(zoomFactor > 1){

        zoomFactor -= 1;

        reDraw(1);
        setLabelsValues();

        if(selected){

            drawRectangle(rect->topLeft().x()*zoomFactor/(zoomFactor+1) , rect->topLeft().y()*zoomFactor/(zoomFactor+1),
                          rect->bottomRight().x()*zoomFactor/(zoomFactor+1) , rect->bottomRight().y()*zoomFactor/(zoomFactor+1));


            ui->graphicsView->horizontalScrollBar()->setValue(rect->topLeft().x()-10);
            ui->graphicsView->verticalScrollBar()->setValue(rect->topLeft().y()-10);
        }
        redoObj *Ob = new redoObj(ZOOM, zoomFactor, previousDeg, cimgx1, cimgy1, cimgx2, cimgy2, gray, ui->graphicsView->horizontalScrollBar()->value(), ui->graphicsView->verticalScrollBar()->value());
        undo.push(Ob);
        emptyRedo();
    }
}

void MainWindow::on_exitButton_clicked()
{
    emptyRedo();
    emptyUndo();

    free(scene);
    free(rect);

    if(start){
        free(originalImage);
        free(image);
    }
    exit(0);
}


void MainWindow::drawRectangle(int x1, int y1, int x2, int y2)
{
    if(!selected || !start)
        return;
    if(x1 > x2){
        int temp = x1;
        x1 = x2;
        x2 = temp;
    }

    if(y1 > y2){
        int temp = y1;
        y1 = y2;
        y2 = temp;
    }

    if(x1<0)
        x1=0;
    if(y1<0)
        y1=0;

    if(x2>scene->width())
        x2=scene->width();

   if(y2>scene->height())
       y2=scene->height();



    QPixmap pixmap;
    if(gray)
       pixmap = QPixmap::fromImage(grayIt());
    else
       pixmap = QPixmap::fromImage(*image);

    QMatrix rm;
    rm.scale(zoomFactor, zoomFactor);
    rm.rotate(previousDeg);
    pixmap = pixmap.transformed(rm);


    QImage tmp(pixmap.toImage());
    QPainter painter(&tmp);
    QPen paintpen(Qt::DashLine);
    paintpen.setWidth(2);
    painter.setPen(paintpen);
    rect= new QRect(x1, y1, x2-x1, y2-y1);
    painter.drawRect(*rect);
    QPixmap newmap = QPixmap::fromImage(tmp);

    scene->clear();
    scene->addPixmap(newmap);
    scene->setSceneRect(newmap.rect());

}

bool MainWindow::eventFilter(QObject* object, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
        QPointF pos = ui->graphicsView->mapToScene(mouse->pos());

        startX = pos.x();
        startY = pos.y();

        return true;

    }else if (event->type() == QEvent::MouseMove){
        QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
        QPointF pos = ui->graphicsView->mapToScene(mouse->pos());

        endX = pos.x();
        endY = pos.y();

        drawRectangle(startX, startY, endX, endY);


        return true;

    }else if (event->type() == QEvent::MouseButtonRelease){
        QMouseEvent* mouse = static_cast<QMouseEvent*>(event);
        QPointF pos = ui->graphicsView->mapToScene(mouse->pos());

        endX = pos.x();
        endY = pos.y();

        drawRectangle(startX, startY, endX, endY);

        return true;
    }
    return false;
}


void MainWindow::on_pushButton_2_clicked()
{
    if(!start)
        return;
    selected ^= true;
    startX = startY = endX = endY = 0;
    rect = NULL;
    reDraw(2);
}


void MainWindow::on_cropButton_clicked()
{
    if(!start || (startX == 0 && startY == 0 && endX == 0 && endY == 0) || !selected)
        return;
    QPixmap pixmap = QPixmap::fromImage(*image);
    QMatrix rm;
    rm.scale(zoomFactor, zoomFactor);
    rm.rotate(previousDeg);
    pixmap = pixmap.transformed(rm);
    QImage tmp(pixmap.toImage());
    QPixmap original = QPixmap::fromImage(tmp);

    QPixmap cropped = original.copy(*rect);
    *image = cropped.toImage();

    if(gray){
        cropped = QPixmap::fromImage(grayIt());
    }else{
        cropped = QPixmap::fromImage(*image);
    }

    scene->clear();
    scene->addPixmap(cropped);
    scene->setSceneRect(cropped.rect());


    cimgx1 = rect->topLeft().x();
    cimgy1 = rect->topLeft().y();
    cimgx2 = rect->bottomRight().x();
    cimgy2 = rect->bottomRight().y();

    redoObj *Ob = new redoObj(CROP, zoomFactor, previousDeg, rect->topLeft().x(), rect->topLeft().y(), rect->bottomRight().x(), rect->bottomRight().y(), gray, ui->graphicsView->horizontalScrollBar()->value(), ui->graphicsView->verticalScrollBar()->value());
    undo.push(Ob);
    emptyRedo();

    resetValues(2);
    reDraw(2);
}

QImage MainWindow::grayIt()
{
    QImage retImg(image->width(),image->height(),QImage::Format_Indexed8);
    QVector<QRgb> table( 256 );
    for( int i = 0; i < 256; ++i )
    {
        table[i] = qRgb(i,i,i);
    }
    retImg.setColorTable(table);

    for(int i =0; i< image->width();i++)
    {
        for(int j=0; j< image->height();j++)
        {
            QRgb value = image->pixel(i,j);
            retImg.setPixel(i,j,qGray(value));
        }

    }

    return retImg;
}

void MainWindow::reDraw(int operation)
{
    QPixmap pixmap;
    if(gray)
        pixmap = QPixmap::fromImage(grayIt());
    else
        pixmap = QPixmap::fromImage(*image);
    QMatrix rm;
    rm.scale(zoomFactor, zoomFactor);
    rm.rotate(ui->horizontalSlider->value());
    pixmap = pixmap.transformed(rm);
    scene->clear();
    scene->addPixmap(pixmap);
    scene->setSceneRect(pixmap.rect());

    if(selected && operation != 1)
       drawRectangle(startX, startY, endX, endY);
}
void MainWindow::resetValues(int operation)
{
    ui->horizontalSlider->setValue(0);
    previousDeg = 0;
    zoomFactor = 1;

    startX = startY = endX = endY = 0;
    selected = false;
    rect = NULL;
    setLabelsValues();
    if(operation == 1)
        gray=false;
}
void MainWindow::setLabelsValues()
{
    QString sAng = QString::number(previousDeg);
    QString sZoom = QString::number(zoomFactor);
    ui->label->setText("Angle: "+sAng);
    ui->label_2->setText("Zoom: "+sZoom+"x");
}
void MainWindow::assignValues()
{
    redoObj *ob = undo.top();
    int op = ob->operation;
    cimgx1 = ob->startX;
    cimgy1 = ob->startY;
    cimgx2 = ob->endX;
    cimgy2 = ob->endY;
    zoomFactor = ob->zoomFactor;
    previousDeg = ob->previousDeg;
    gray = ob->gray;
    ui->horizontalSlider->setValue(previousDeg);
    selected = false;
    rect = NULL;

    if(op == CROP)
    {
        recrop();
    }
    else if(op == ZOOM)
    {
        reDraw(1);
    }
    else if(op == GRAY)
    {
        reDraw(1);
    }
    else if(op == ROTATE)
    {
        reDraw(1);
    }
    else{
        reDraw(1);
    }
    setLabelsValues();
    ui->graphicsView->horizontalScrollBar()->setValue(ob->hslider);
    ui->graphicsView->verticalScrollBar()->setValue(ob->vslider);
}
void MainWindow::recrop()
{
    if(cimgx1 == 0 && cimgx2 == 0 && cimgy1 == 0 && cimgy2 == 0)
    {
        QPixmap pixmap = QPixmap::fromImage(*originalImage);
        *image = pixmap.toImage();
        reDraw(1);
        return;
    }
    QPixmap pixmap = QPixmap::fromImage(*image);
    QMatrix rm;
    rm.scale(zoomFactor, zoomFactor);
    rm.rotate(previousDeg);
    pixmap = pixmap.transformed(rm);
    QImage tmp(pixmap.toImage());
    QPixmap original = QPixmap::fromImage(tmp);

    rect = new QRect(cimgx1, cimgy1, cimgx2-cimgx1, cimgy2 - cimgy1);
    QPixmap cropped = original.copy(*rect);
    *image = cropped.toImage();

    if(gray){
        cropped = QPixmap::fromImage(grayIt());
    }else{
        cropped = QPixmap::fromImage(*image);
    }

    scene->clear();
    scene->addPixmap(cropped);
    scene->setSceneRect(cropped.rect());
    resetValues(2);
}
void MainWindow::emptyRedo()
{
    while(redo.size() > 0){
        redo.pop();
    }
}

void MainWindow::emptyUndo()
{
    while(undo.size() > 0){
        undo.pop();
    }
}

void MainWindow::on_horizontalSlider_sliderReleased()
{
    redoObj *Ob = new redoObj(ROTATE, zoomFactor, previousDeg, cimgx1, cimgy1, cimgx2, cimgy2, gray, ui->graphicsView->horizontalScrollBar()->value(), ui->graphicsView->verticalScrollBar()->value());
    undo.push(Ob);
    emptyRedo();
}

void MainWindow::on_reset_clicked()
{
    if(undo.size() == 0 || !start)
        return;
    while(undo.size() > 1){
        undo.pop();
    }
    while(redo.size() > 0){
        redo.pop();
    }
    QPixmap pixmap = QPixmap::fromImage(*originalImage);
    *image = pixmap.toImage();
    assignValues();

}

void MainWindow::on_undoAction_clicked()
{
    if(undo.size() == 0 || !start)
        return;
    if(undo.size() == 1){
        assignValues();
        return;
    }
    QPixmap pixmap = QPixmap::fromImage(*originalImage);
    *image = pixmap.toImage();
    std::stack <redoObj *> temp;
    while(undo.size() > 0){
        redoObj *ob = undo.top();
        undo.pop();
        temp.push(ob);
    }
    while(temp.size() > 1)
    {
        redoObj *ob = temp.top();
        temp.pop();
        undo.push(ob);
        assignValues();
    }
    redoObj *obj = temp.top();
    temp.pop();
    redo.push(obj);

}

void MainWindow::on_redoAction_clicked()
{
    if(redo.size() == 0 || !start)
        return;
    redoObj *obj = redo.top();
    redo.pop();
    undo.push(obj);
    assignValues();
}
