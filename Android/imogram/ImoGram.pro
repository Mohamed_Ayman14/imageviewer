#-------------------------------------------------
#
# Project created by QtCreator 2015-06-21T14:30:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImoGram
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    redoobj.cpp

HEADERS  += mainwindow.h \
    redoobj.h

FORMS    += mainwindow.ui

RESOURCES += \
    firstphoto.qrc

DISTFILES += \
    ../build-ImoGram-Desktop_Qt_5_4_2_MinGW_32bit-Debug/shosh.jpg \
    ../Pictures/close4.png \
    ../Pictures/crop1.png \
    ../Pictures/open1.png \
    ../Pictures/reset4.png \
    ../Pictures/save.png \
    ../Pictures/Zin.png \
    ../Pictures/Zout.png
