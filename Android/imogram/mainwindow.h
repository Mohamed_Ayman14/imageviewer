#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QGraphicsScene>
#include <QWheelEvent>
#include <stack>
#include "redoobj.h"




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
     Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QGraphicsScene *scene;
    QImage *originalImage;
    QImage *image;
    QPixmap pix_image;
    qreal zoomFactor;
    int previousDeg;
    QRect *rect;
    int startX, startY, endX, endY;
    bool selected;
    bool gray, start;
    std::stack <redoObj *> undo;
    std::stack <redoObj *> redo;

    int cimgx1, cimgy1, cimgx2, cimgy2;

private slots:
    void on_pushButton_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_2_clicked();
    void on_cropButton_clicked();
    void on_pushButton_7_clicked();
    void on_exitButton_clicked();

    void addImage(QString path);
    void rotateImg(int);
    void reDraw(int);
    void resetValues(int);
    void setLabelsValues();
    void drawRectangle(int, int, int, int);
    void assignValues();
    void recrop();
    void emptyRedo();
	void emptyUndo();

    bool eventFilter(QObject* object, QEvent* event);
    QImage grayIt();

    void on_horizontalSlider_sliderReleased();
    void on_reset_clicked();
    void on_undoAction_clicked();

    void on_redoAction_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
