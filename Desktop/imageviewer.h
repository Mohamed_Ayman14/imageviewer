#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include <QImage>
#include <QGraphicsView>
#include <QRubberBand>
#include <stack>
#include <QPixmap>

#include "action.h"

class MenuBar;

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QColor;

class ImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    ImageViewer();
    bool loadFile(const QString &);
    void open();
    void saveAs();
    void copy();
    void paste();
    void rotateLeft();
    void rotateRight();
    void crop();
    void zoomIn();
    void zoomOut();
    void resetScale();
    void fitToWindow();
    void about();
    void cropAction();
    void resetToOriginal();
    void undo();
    void redo();

    void dummyRotate();

private:
    void recordAction(Action*);
    void reloadImageByActions();

    void rotate();

    void clearHistory();

    Action* executeCrop();
    Action* executeZoomIn();
    Action* executeZoomOut();
    Action* executeResetScale();
    Action* executeRotate(int);

    int getDegreeChange();


    void simulateRotate(int);
    void createMenus();
    void updateActions();
    bool saveFile(const QString &fileName);
    void setImage(const QImage &newImage);
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void drawRectangle();

    void updateEnabledStatus();

    MenuBar* _menuBar;

    std::stack<Action*> previousActions, nextActions;

    QImage image;
    QImage originalImage;
    QLabel *imageLabel;
    QScrollArea *scrollArea;

    double hScaleFactor;
    double vScaleFactor;

    QPoint topleft;
    QPoint bottomright;
    QPoint movingPoint;
    QRubberBand *rubberBand;

    int degree;
    bool holdingAction;
};

#endif
