#ifndef ACTION_H
#define ACTION_H


enum class ActionType { CROP, ROTATE, ZOOM_IN, ZOOM_OUT, RESET_SCALE};
class Action
{
public:
    Action();

    void setCropState(int, int, int, int);
    void setRotateState(int);
    void setZoomIn();
    void setZoomOut();
    void setResetScale();

    int getRotationAngle();

    int getTopLeftX();
    int getTopLeftY();
    int getBottomRightX();
    int getBottomRightY();

    ActionType getActionType();


private:
    ActionType _actionType;
    int _upperX;
    int _upperY;
    int _lowerX;
    int _lowerY;
    int _rotationAngle;
};

#endif // ACTION_H
