#include <QtWidgets>
#include <QRubberBand>

#include "imageviewer.h"
#include "menubar.h"

ImageViewer::ImageViewer()
   : imageLabel(new QLabel)
   , scrollArea(new QScrollArea)
   , hScaleFactor(1)
   , vScaleFactor(1)
{
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    rubberBand = new QRubberBand(QRubberBand::Rectangle, imageLabel);

    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    scrollArea->setVisible(false);

    setCentralWidget(scrollArea);

    _menuBar = new MenuBar(this);

    resize(QGuiApplication::primaryScreen()->availableSize() * 4 / 5);
}


bool ImageViewer::loadFile(const QString &fileName)
{
    QImageReader reader(fileName);
    reader.setAutoTransform(true);
    const QImage newImage = reader.read();
    if (newImage.isNull()) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot load %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
        return false;
    }

    setImage(newImage);
    originalImage = newImage.copy();
    setWindowFilePath(fileName);

    const QString message = tr("Opened \"%1\", %2x%3, Depth: %4")
        .arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());
    statusBar()->showMessage(message);
    return true;
}

void ImageViewer::setImage(const QImage &newImage)
{
    image = newImage;
    imageLabel->setPixmap(QPixmap::fromImage(image));
    hScaleFactor = 1.0;
    vScaleFactor = 1.0;

    scrollArea->setVisible(true);
    _menuBar->setFitToWindowEnabled(true);
    updateActions();

    if(!_menuBar->isFitToWindowChecked())
        imageLabel->adjustSize();
}


bool ImageViewer::saveFile(const QString &fileName)
{
    QImageWriter writer(fileName);

    if (!writer.write(image)) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot write %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName)), writer.errorString());
        return false;
    }
    const QString message = tr("Wrote \"%1\"").arg(QDir::toNativeSeparators(fileName));
    statusBar()->showMessage(message);
    return true;
}


static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode)
{
    static bool firstDialog = true;

    if (firstDialog) {
        firstDialog = false;
        const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
        dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
    }

    QStringList mimeTypeFilters;
    const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
        ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();
    foreach (const QByteArray &mimeTypeName, supportedMimeTypes)
        mimeTypeFilters.append(mimeTypeName);
    mimeTypeFilters.sort();
    dialog.setMimeTypeFilters(mimeTypeFilters);
    dialog.selectMimeTypeFilter("image/jpeg");
    if (acceptMode == QFileDialog::AcceptSave)
        dialog.setDefaultSuffix("jpg");
}

void ImageViewer::open()
{
    QFileDialog dialog(this, tr("Open File"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptOpen);

    while (dialog.exec() == QDialog::Accepted && !loadFile(dialog.selectedFiles().first())) {}
    degree = 0;
    clearHistory();
}

void ImageViewer::saveAs()
{
    QFileDialog dialog(this, tr("Save File As"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptSave);

    while (dialog.exec() == QDialog::Accepted && !saveFile(dialog.selectedFiles().first())) {}
}

void ImageViewer::copy()
{
#ifndef QT_NO_CLIPBOARD
    QGuiApplication::clipboard()->setImage(image);
#endif // !QT_NO_CLIPBOARD
}

#ifndef QT_NO_CLIPBOARD
static QImage clipboardImage()
{
    if (const QMimeData *mimeData = QGuiApplication::clipboard()->mimeData()) {
        if (mimeData->hasImage()) {
            const QImage image = qvariant_cast<QImage>(mimeData->imageData());
            if (!image.isNull())
                return image;
        }
    }
    return QImage();
}
#endif // !QT_NO_CLIPBOARD

void ImageViewer::paste()
{
#ifndef QT_NO_CLIPBOARD
    const QImage newImage = clipboardImage();
    if (newImage.isNull()) {
        statusBar()->showMessage(tr("No image in clipboard"));
    } else {
        setImage(newImage);
        setWindowFilePath(QString());
        const QString message = tr("Obtained image from clipboard, %1x%2, Depth: %3")
            .arg(newImage.width()).arg(newImage.height()).arg(newImage.depth());
        statusBar()->showMessage(message);
    }
#endif // !QT_NO_CLIPBOARD
}

void ImageViewer::rotate()
{
    int degreeChange = getDegreeChange();
    recordAction(executeRotate(degreeChange));
}


int ImageViewer::getDegreeChange() {
    int w = image.width();
    int h = image.height();
    QPoint shiftedTopLeft(topleft.x() - w, topleft.y() - h);
    QPoint shiftedBottomRight(bottomright.x() - w, bottomright.y() - h);

    int x1 = shiftedTopLeft.x();
    int x2 = shiftedBottomRight.x();
    int y1 = shiftedTopLeft.y();
    int y2 = shiftedBottomRight.y();
    double dot = x1*x2 + y1*y2;
    double det = x1*y2 - y1*x2;
    double angle = atan2(det, dot);
    return angle * 180 / M_PI;
}

Action* ImageViewer::executeRotate(int degreeChange) {
    degree = (degree + degreeChange + 360) % 360;
    QPixmap pixmap(QPixmap::fromImage(image));
    QTransform rm;
    rm.rotate(degree);
    pixmap = pixmap.transformed(rm);
    imageLabel->setPixmap(pixmap);
    imageLabel->adjustSize();
    Action* action = new Action;
    action->setRotateState(degreeChange);
    return action;
}

void ImageViewer::simulateRotate(int degreeChange) {
    int tempdegree = (degree + degreeChange + 360) % 360;
    QPixmap pixmap(QPixmap::fromImage(image));
    QTransform rm;
    rm.rotate(tempdegree);
    pixmap = pixmap.transformed(rm);
    imageLabel->setPixmap(pixmap);
    imageLabel->adjustSize();
}

void ImageViewer::dummyRotate() {
    _menuBar->setRotateChecked(true);
}

void ImageViewer::crop()
{
    recordAction(executeCrop());
}

Action* ImageViewer::executeCrop() {
    if (topleft.rx() > bottomright.rx() && topleft.ry() > bottomright.ry()){
        QPoint tmp = bottomright;
        bottomright = topleft;
        topleft = tmp;
    }
    if (topleft.rx() > bottomright.rx() && topleft.ry() < bottomright.ry()){
        int tmp = bottomright.rx();
        bottomright.setX(topleft.rx());
        topleft.setX(tmp);
    }
    if (topleft.rx() < bottomright.rx() && topleft.ry() > bottomright.ry()){
        int tmp = bottomright.ry();
        bottomright.setY(topleft.ry());
        topleft.setY(tmp);
    }
    topleft.setX((topleft.rx() + scrollArea->horizontalScrollBar()->value()) / hScaleFactor);
    topleft.setY((topleft.ry() + scrollArea->verticalScrollBar()->value() - 22) / vScaleFactor);
    bottomright.setX((bottomright.rx() + scrollArea->horizontalScrollBar()->value()) / hScaleFactor);
    bottomright.setY((bottomright.ry() + scrollArea->verticalScrollBar()->value() - 22) / vScaleFactor);
    if (_menuBar->isFitToWindowChecked()) {
        double hScale, vScale;
        hScale = imageLabel->width() * 1.0 / image.width();
        vScale = imageLabel->height()* 1.0 / image.height();
        topleft.setX(topleft.rx() / hScale);
        topleft.setY(topleft.ry() / vScale);
        bottomright.setX(bottomright.rx() / hScale);
        bottomright.setY(bottomright.ry() / vScale);
    }
    QRect rec(topleft, bottomright);
    image = imageLabel->pixmap()->toImage().copy(rec);
    setImage(image);
    Action* action = new Action;
    action->setCropState(topleft.rx(), topleft.ry(), bottomright.rx(), bottomright.ry());
    return action;
}

void ImageViewer::zoomIn()
{
    recordAction(executeZoomIn());
}

void ImageViewer::zoomOut()
{
    recordAction(executeZoomOut());
}

Action* ImageViewer::executeZoomIn() {
    scaleImage(1.25);
    Action* action = new Action;
    action->setZoomIn();
    return action;
}

Action* ImageViewer::executeZoomOut() {
    scaleImage(0.8);
    Action* action = new Action;
    action->setZoomIn();
    return action;
}

void ImageViewer::clearHistory() {
    while(!previousActions.empty()) previousActions.pop();
    while(!nextActions.empty()) nextActions.pop();
    _menuBar->setRedoEnabled(false);
    _menuBar->setUndoEnabled(false);
    _menuBar->setResetEnabled(false);
}

void ImageViewer::resetScale()
{
    recordAction(executeResetScale());
}

Action* ImageViewer::executeResetScale() {
    imageLabel->adjustSize();
    vScaleFactor = 1.0;
    hScaleFactor = 1.0;
    Action* action = new Action;
    action->setResetScale();
    return action;
}

void ImageViewer::fitToWindow()
{
    bool fitToWindow = _menuBar->isFitToWindowChecked();
    scrollArea->setWidgetResizable(fitToWindow);
    if (!fitToWindow)
        executeResetScale();
    updateActions();
}

void ImageViewer::about()
{
    QMessageBox::about(this, tr("About Image Viewer"),
            tr("<p>The <b>Image Viewer</b> was developed by Dahlia Chehata "));
}

void ImageViewer::updateActions()
{
    _menuBar->updateActs(image);
}

void ImageViewer::scaleImage(double hFactor, double vFactor)
{
    Q_ASSERT(imageLabel->pixmap());

    hScaleFactor *= hFactor;
    vScaleFactor *= vFactor;

    imageLabel->resize(hScaleFactor * imageLabel->pixmap()->size().width(),
                       vScaleFactor * imageLabel->pixmap()->size().height());

    adjustScrollBar(scrollArea->horizontalScrollBar(), hFactor);
    adjustScrollBar(scrollArea->verticalScrollBar(), vFactor);

    _menuBar->setZoomInEnabled(vScaleFactor < 3.0 && hScaleFactor < 3.0);
    _menuBar->setZoomOutEnabled(vScaleFactor > 0.27 && hScaleFactor > 0.27);
}

void ImageViewer::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

void ImageViewer::mousePressEvent(QMouseEvent *event)
{
    if(_menuBar->isCropChecked() || _menuBar->isRotateChecked())
    {
        holdingAction = true;
        topleft = event->pos();
    }
}

void ImageViewer::mouseReleaseEvent(QMouseEvent *event)
{
    if(holdingAction) {
        bottomright = event->pos();
        if(_menuBar->isCropChecked())
        {
            crop();
            _menuBar->setCropChecked(false);
            rubberBand->hide();
        } else if(_menuBar->isRotateChecked()) {
            rotate();
            _menuBar->setRotateChecked(false);
        }
        holdingAction = false;
    }
}

void ImageViewer::mouseMoveEvent(QMouseEvent *event)
{
    if(holdingAction) {
        if (_menuBar->isCropChecked())
        {
            movingPoint = event->pos();
            drawRectangle();
        } else if(_menuBar->isRotateChecked()) {
            bottomright = event->pos();
            int degreeChange = getDegreeChange();
            simulateRotate(degreeChange);
        }
    }
}

void ImageViewer::drawRectangle()
{
    QPoint tmp = topleft;
    if (tmp.rx() > movingPoint.rx() && tmp.ry() > movingPoint.ry()){
        QPoint tmp2 = movingPoint;
        movingPoint = tmp2;
        tmp = tmp2;
    }
    if (tmp.rx() > movingPoint.rx() && tmp.ry() < movingPoint.ry()){
        int tmp2 = movingPoint.rx();
        movingPoint.setX(tmp.rx());
        tmp.setX(tmp2);
    }
    if (tmp.rx() < movingPoint.rx() && tmp.ry() > movingPoint.ry()){
        int tmp2 = movingPoint.ry();
        movingPoint.setY(tmp.ry());
        tmp.setY(tmp2);
    }
    tmp.setX(tmp.rx() + scrollArea->horizontalScrollBar()->value());
    tmp.setY(tmp.ry() + scrollArea->verticalScrollBar()->value() - 22);
    movingPoint.setX(movingPoint.rx() + scrollArea->horizontalScrollBar()->value());
    movingPoint.setY(movingPoint.ry() + scrollArea->verticalScrollBar()->value() - 22);
    if (!rubberBand)
        rubberBand = new QRubberBand(QRubberBand::Rectangle, imageLabel);
    rubberBand->setGeometry(QRect(tmp, movingPoint));
    rubberBand->show();
}

void ImageViewer::cropAction()
{
    _menuBar->setCropChecked(_menuBar->isCropChecked());
}

void ImageViewer::resetToOriginal() {
    while(!previousActions.empty()) {
        nextActions.push(previousActions.top());
        previousActions.pop();
    }
    updateEnabledStatus();
    reloadImageByActions();
}

void ImageViewer::recordAction(Action *action) {
    while(!nextActions.empty()) {
        Action* action = nextActions.top();
        nextActions.pop();
        delete action;
    }
    previousActions.push(action);
    updateEnabledStatus();
}

void ImageViewer::undo() {
    nextActions.push(previousActions.top());
    previousActions.pop();
    updateEnabledStatus();
    reloadImageByActions();
}

void ImageViewer::redo() {
    previousActions.push(nextActions.top());
    nextActions.pop();
    updateEnabledStatus();
    reloadImageByActions();
}

void ImageViewer::updateEnabledStatus() {
    _menuBar->setRedoEnabled(!nextActions.empty());
    _menuBar->setUndoEnabled(!previousActions.empty());
    _menuBar->setResetEnabled(!previousActions.empty());
}

void ImageViewer::reloadImageByActions() {
    std::stack<Action*> reversedActions;
    while(!previousActions.empty()) {
        reversedActions.push(previousActions.top());
        previousActions.pop();
    }
    double scale = 1.0;
    image = originalImage.copy();
    QPixmap originalPix(QPixmap::fromImage(originalImage.copy()));
    degree = 0;
    while(!reversedActions.empty()) {
        Action* action = reversedActions.top();
        if(action->getActionType() == ActionType::CROP) {
            QPoint currentTopLeft = QPoint(action->getTopLeftX(), action->getTopLeftY());
            QPoint currentBottomRight = QPoint(action->getBottomRightX(), action->getBottomRightY());
            QRect rec(currentTopLeft, currentBottomRight);
            QPixmap tempo(QPixmap::fromImage(originalPix.toImage().copy(rec)));
            originalPix = tempo;
        } else if(action->getActionType() == ActionType::RESET_SCALE) {
            scale = 1.0;
        } else if(action->getActionType() == ActionType::ROTATE) {
            QPixmap pixmap(QPixmap::fromImage(originalPix.toImage()));
            QTransform rm;
            degree += action->getRotationAngle();
            degree = (degree + 360) % 360;
            rm.rotate(degree);
            originalPix = pixmap.transformed(rm);
        } else if(action->getActionType() == ActionType::ZOOM_IN) {
            scale *= 1.25;
        } else if(action->getActionType() == ActionType::ZOOM_OUT) {
            scale *= 0.8;
        }
        previousActions.push(action);
        reversedActions.pop();
    }
    image = originalPix.toImage();
    setImage(image);
    scaleImage(scale);
}
