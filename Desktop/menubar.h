#ifndef MENUBARINITIALIZER_H
#define MENUBARINITIALIZER_H

#include <QMainWindow>
#include <QImage>
#include <QGraphicsView>
#include <QRubberBand>
#include <QtWidgets>
#include "imageviewer.h"

class MenuBar
{
public:
    MenuBar(ImageViewer*);
    void updateActs(QImage);

    bool isCropChecked();
    bool isFitToWindowChecked();
    bool isZoomInChecked();
    bool isZoomOutChecked();
    bool isRotateChecked();

    void setCropChecked(bool);
    void setFitToWindowChecked(bool);
    void setZoomInChecked(bool);
    void setZoomOutChecked(bool);
    void setRotateChecked(bool);

    void setCropEnabled(bool);
    void setFitToWindowEnabled(bool);
    void setZoomInEnabled(bool);
    void setZoomOutEnabled(bool);
    void setUndoEnabled(bool);
    void setRedoEnabled(bool);
    void setResetEnabled(bool);
    void setRotateEnabled(bool);

private:
    void initializeFileMenu(QMenu*, ImageViewer*);
    void initializeEditMenu(QMenu*, ImageViewer*);
    void initializeViewMenu(QMenu*, ImageViewer*);
    void initializeHelpMenu(QMenu*, ImageViewer*);

    QAction *saveAsAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *rotate;
    QAction *cropAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *resetAct;
    QAction *fitToWindowAct;
    QAction *resetToOriginalAct;
    QAction *undoAct;
    QAction *redoAct;
};

#endif // MENUBARINITIALIZER_H
