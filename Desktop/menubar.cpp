#include "menubar.h"

MenuBar::MenuBar(ImageViewer* window)
{
    QMenu *fileMenu = window->menuBar()->addMenu(window->tr("&File"));
    initializeFileMenu(fileMenu, window);

    QMenu *editMenu = window->menuBar()->addMenu(window->tr("&Edit"));
    initializeEditMenu(editMenu, window);

    QMenu *viewMenu = window->menuBar()->addMenu(window->tr("&View"));
    initializeViewMenu(viewMenu, window);

    QMenu *helpMenu = window->menuBar()->addMenu(window->tr("&Help"));
    initializeHelpMenu(helpMenu, window);
}

void MenuBar::initializeFileMenu(QMenu* fileMenu, ImageViewer* window) {
    QAction *openAct = fileMenu->addAction(window->tr("&Open..."), window, &ImageViewer::open);
    openAct->setShortcut(QKeySequence::Open);

    saveAsAct = fileMenu->addAction(window->tr("&Save As..."), window, &ImageViewer::saveAs);
    saveAsAct->setShortcut(window->tr("Ctrl+S"));
    saveAsAct->setEnabled(false);

    fileMenu->addSeparator();

    QAction *exitAct = fileMenu->addAction(window->tr("E&xit"), window, &QWidget::close);
    exitAct->setShortcut(window->tr("Ctrl+Q"));
}

void MenuBar::initializeEditMenu(QMenu* editMenu, ImageViewer* window) {

    copyAct = editMenu->addAction(window->tr("&Copy"), window, &ImageViewer::copy);
    copyAct->setShortcut(QKeySequence::Copy);
    copyAct->setEnabled(false);

    pasteAct = editMenu->addAction(window->tr("&Paste"), window, &ImageViewer::paste);
    pasteAct->setShortcut(QKeySequence::Paste);

    editMenu->addSeparator();

    rotate = editMenu->addAction(window->tr("&Rotate"), window, &ImageViewer::dummyRotate);
    rotate->setShortcut(window->tr("Ctrl+R"));
    rotate->setCheckable(true);
    rotate->setEnabled(false);

    editMenu->addSeparator();

    cropAct = editMenu->addAction(window->tr("&Crop"), window, &ImageViewer::cropAction);
    cropAct->setEnabled(false);
    cropAct->setCheckable(true);
    cropAct->setShortcut(window->tr("Ctrl+U"));

    editMenu->addSeparator();

    resetToOriginalAct = editMenu->addAction(window->tr("&Reset to Origin"), window, &ImageViewer::resetToOriginal);
    resetToOriginalAct->setEnabled(false);
    resetToOriginalAct->setShortcut(window->tr("Ctrl+K"));

    undoAct = editMenu->addAction(window->tr("&Undo"), window, &ImageViewer::undo);
    undoAct->setEnabled(false);
    undoAct->setShortcut(window->tr("Ctrl+Z"));

    redoAct = editMenu->addAction(window->tr("&Redo"), window, &ImageViewer::redo);
    redoAct->setEnabled(false);
    redoAct->setShortcut(window->tr("Ctrl+Shift+Z"));

}

void MenuBar::initializeViewMenu(QMenu* viewMenu, ImageViewer* window) {
    zoomInAct = viewMenu->addAction(window->tr("Zoom &In (25%)"), window, &ImageViewer::zoomIn);
    zoomInAct->setShortcut(QKeySequence::ZoomIn);
    zoomInAct->setEnabled(false);

    zoomOutAct = viewMenu->addAction(window->tr("Zoom &Out (25%)"), window, &ImageViewer::zoomOut);
    zoomOutAct->setShortcut(QKeySequence::ZoomOut);
    zoomOutAct->setEnabled(false);

    viewMenu->addSeparator();

    resetAct = viewMenu->addAction(window->tr("&Reset"), window, &ImageViewer::resetScale);
    resetAct->setShortcut(window->tr("Ctrl+N"));
    resetAct->setEnabled(false);

    viewMenu->addSeparator();

    fitToWindowAct = viewMenu->addAction(window ->tr("&Fit to Window"), window, &ImageViewer::fitToWindow);
    fitToWindowAct->setEnabled(false);
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setShortcut(window->tr("Ctrl+F"));
}

void MenuBar::initializeHelpMenu(QMenu* helpMenu, ImageViewer* window) {
    helpMenu->addAction(window->tr("&About"), window, &ImageViewer::about);
}

void MenuBar::updateActs(QImage image) {
    saveAsAct->setEnabled(!image.isNull());
    copyAct->setEnabled(!image.isNull());
    zoomInAct->setEnabled(!fitToWindowAct->isChecked());
    zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
    resetAct->setEnabled(!fitToWindowAct->isChecked());
    rotate->setEnabled(!image.isNull());
    cropAct->setEnabled(!image.isNull());
}

bool MenuBar::isCropChecked() {
    return cropAct->isChecked();
}

bool MenuBar::isFitToWindowChecked() {
    return fitToWindowAct->isChecked();
}

bool MenuBar::isZoomInChecked() {
    return zoomInAct->isChecked();
}

bool MenuBar::isZoomOutChecked() {
    return zoomOutAct->isChecked();
}

bool MenuBar::isRotateChecked() {
    return rotate->isChecked();
}

void MenuBar::setCropChecked(bool checkedStatus) {
    cropAct->setChecked(checkedStatus);
    if(checkedStatus) {
        rotate->setChecked(false);
    }
}

void MenuBar::setFitToWindowChecked(bool checkedStatus) {
    fitToWindowAct->setChecked(checkedStatus);
}

void MenuBar::setZoomInChecked(bool checkedStatus) {
    zoomInAct->setChecked(checkedStatus);
}

void MenuBar::setZoomOutChecked(bool checkedStatus) {
    zoomOutAct->setChecked(checkedStatus);
}

void MenuBar::setRotateChecked(bool checkedStatus) {
    rotate->setChecked(checkedStatus);
    if(checkedStatus) {
        cropAct->setChecked(false);
    }
}


void MenuBar::setCropEnabled(bool checkedStatus) {
    cropAct->setEnabled(checkedStatus);
}

void MenuBar::setFitToWindowEnabled(bool enabledStatus) {
    fitToWindowAct->setEnabled(enabledStatus);
}

void MenuBar::setZoomInEnabled(bool enabledStatus) {
    zoomInAct->setEnabled(enabledStatus);
}

void MenuBar::setZoomOutEnabled(bool enabledStatus) {
    zoomOutAct->setEnabled(enabledStatus);
}

void MenuBar::setUndoEnabled(bool enabledStatus) {
    undoAct->setEnabled(enabledStatus);
}

void MenuBar::setRedoEnabled(bool enabledStatus) {
    redoAct->setEnabled(enabledStatus);
}

void MenuBar::setResetEnabled(bool enabledStatus) {
    resetToOriginalAct->setEnabled(enabledStatus);
}

void MenuBar::setRotateEnabled(bool enabledStatus) {
    rotate->setEnabled(enabledStatus);
}
