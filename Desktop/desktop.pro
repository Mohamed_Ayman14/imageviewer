QT += widgets

HEADERS       = \
                imageviewer.h \
    menubar.h \
    action.h
SOURCES       = \
                main.cpp \
                imageviewer.cpp \
    menubar.cpp \
    action.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/mainwindows/test
INSTALLS += target
