#include "action.h"

Action::Action()
{

}

void Action::setCropState(int upX, int upY, int lowX, int lowY) {
    _actionType = ActionType::CROP;
    _upperX = upX;
    _upperY = upY;
    _lowerX = lowX;
    _lowerY = lowY;
}
void Action::setRotateState(int rotationAngle) {
    _actionType = ActionType::ROTATE;
    _rotationAngle = rotationAngle;
}
void Action::setZoomIn() {
    _actionType = ActionType::ZOOM_IN;
}
void Action::setZoomOut() {
    _actionType = ActionType::ZOOM_OUT;
}

void Action::setResetScale() {
    _actionType = ActionType::RESET_SCALE;
}

ActionType Action::getActionType() {
    return _actionType;
}

int Action::getRotationAngle() {
    return _rotationAngle;
}

int Action::getTopLeftX() {
    return _upperX;
}

int Action::getTopLeftY() {
    return _upperY;
}

int Action::getBottomRightX() {
    return _lowerX;
}

int Action::getBottomRightY() {
    return _lowerY;
}
